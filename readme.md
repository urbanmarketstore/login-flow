# Saga Login Flow

> A Login/Register Demo r flow built with React & Redux Saga


This application demonstrates what a React-based register/login workflow might look like with 

## Authentication

Authentication happens in `app/auth/index.js`, using `fakeRequest.js` and `fakeServer.js`. `fakeRequest` is a fake `XMLHttpRequest` wrapper. `fakeServer` responds to the fake HTTP requests and pretends to be a real server, storing the current users in local storage with the passwords encrypted using `bcrypt`.

## Thanks

* [Max Stoiber](https://twitter.com/mxstbr) for the Login Flow idea.
* [Yassine Elouafi](https://github.com/yelouafi) for Redux Saga. Awesome!

## License

MIT © [Mcdave Umoh]
