import React from 'react'

function Dashboard () {
  return (
    <article>
      <section className='text-section'>
        <h1>Dashboard</h1>
        <p>
          Welcome, you are logged in!
          visit our site with the Link
          <a href='#'> Naija Loaded</a>.
        </p>
      </section>
    </article>
  )
}

export default Dashboard
