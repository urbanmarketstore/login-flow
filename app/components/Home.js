import React, {Component} from 'react'
import {connect} from 'react-redux'

class Home extends Component {
  render () {
    return (
      <article>
        <div>
          <section className='text-section'>
            <h1>Welcome to the Login / Register Demo</h1>
            <p>This application demonstrates a simple Register / Login Function
               </p>

            <p>Try logging in with username <code> umoh </code> and password 
            <code> password </code>, as so to check out the application then register others as well.
             They'll be saved in local storage so they'll persist across page reloads.</p>
          </section>
        </div>
      </article>
    )
  }
}

function select (state) {
  return {
    data: state
  }
}

export default connect(select)(Home)
